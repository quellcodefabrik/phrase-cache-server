# phrase-microservice

This package integrates the Phrase API and implements a simple cache to not exceed the Phrase API's rate limits.

## Setup

Run `npm install` to get all required dependencies.

## Make it run

Now start the script with `node index.js`. You can retrieve your translations with a simple GET request to `http://localhost:3000/api/translations/:locale` where `:locale` is one of the locales that you have defined in your Phrase project.
