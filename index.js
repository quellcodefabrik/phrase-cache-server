require('dotenv').config();

const fastify = require('fastify');
const bent = require('bent');

var { DateTime, Interval } = require('luxon');

const phraseBaseUrl = 'https://api.phrase.com/v2';

const app = fastify();
const getJSON = bent('json');

const cache = {};

app.get('/api/translations/:locale', async (req, reply) => {
  const { locale } = req.params;

  if (!locale) {
    return reply
      .code(400)
      .header('Content-Type', 'application/json; charset=utf-8')
      .send({ message: '"Locale" parameter is missing from the URL.' });
  }

  // check cache first
  if (cache[locale]) {
    const timeStamp = DateTime.fromISO(cache[locale].timestamp);
    const minuteDifference = Interval.fromDateTimes(timeStamp, DateTime.local()).count('minutes');

    if (minuteDifference < 5) {
      console.log('Translations served from cache.');
      return cache[locale];
    }
  }

  try {
    console.log('Making request to Phrase API.');

    const translations = await getJSON(`${phraseBaseUrl}/projects/${process.env.PHRASE_PROJECT_ID}/locales/${locale}/download?file_format=i18next`, null, {
      Authorization: `token ${process.env.PHRASE_ACCESS_TOKEN}`
    });

    // save to cache
    cache[locale] = {
      status: 'cached',
      timestamp: DateTime.local().toISOTime(),
      translations
    };

    return {
      ...cache[locale],
      status: 'ok'
    };
  } catch (ex) {
    // if there is an error for the given local we should also save
    // it to the cache to avoid making the failing request too often
    cache[locale] = {
      status: 'not_ok',
      timestamp: DateTime.local().toISOTime(),
      translations: {}
    };

    return await ex.json();
  }
});

app.listen(3000).then(() => {
  console.log('Server running at http://localhost:3000/');
});
